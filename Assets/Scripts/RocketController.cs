﻿using UnityEngine;
using System.Collections;

public class RocketController : MonoBehaviour, iInputManager {

	private GameObject rocketShip;
	private Vector2 force = new Vector2(9.81f, 0.0f);
	private float velocityX, maxVelocityX, maxRotation;

	#region iInputManager implementation
	public void OnTouchBegan ()
	{
		force = force * -1;
	}
	public void OnTouchEnded ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchMoved ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchStayed ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchBeganAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchEndedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchMovedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchStayedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	#endregion

	// Use this for initialization
	void Start () 
	{
		rocketShip = GameObject.Find ("RocketShip");
		maxRotation = 30.0f;
		maxVelocityX = 10.0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		velocityX = rocketShip.rigidbody2D.velocity.x;

		if(rocketShip == null && Grid.gameManager.gameStarted)
		{
			rocketShip = GameObject.Find ("RocketShip");
		}

		if(Grid.gameManager.gameStarted)
		{
			rocketShip.rigidbody2D.AddForce(force * rocketShip.rigidbody2D.mass);
		}

		if(velocityX > 0.0f && velocityX < 10.0f)
		{
			float velPercentage = velocityX/maxVelocityX;
			rocketShip.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, velPercentage * maxRotation * -1.0f));
		} else if (velocityX >= 10.0f)
		{
			rocketShip.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, maxRotation * -1.0f));
		}

		if(velocityX < 0.0f && velocityX > -10.0f)
		{
			float velPercentage = -velocityX/maxVelocityX;
			rocketShip.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, velPercentage * maxRotation));
		} else if (velocityX <= -10.0f)
		{
			rocketShip.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, maxRotation));
		}
	}
}
