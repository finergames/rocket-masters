﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public bool gameStarted;

	// Use this for initialization
	void Start () {
		Grid.gameManager.gameStarted = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int Multiplier()
	{
		if(Random.value > 0.5f)
		{
			return -1;
		}else
		{
			return 1;
		}
	}
}
