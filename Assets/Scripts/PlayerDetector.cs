﻿using UnityEngine;
using System.Collections;

public class PlayerDetector : MonoBehaviour {
	
	public GameObject enemy;
	private GameObject wallLeft, wallRight;
	private float objectSpeed = 2.0f;
	private int maxNumEnemies = 3;

	void Awake()
	{
//		stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));
		
		wallLeft = GameObject.Find ("WallLeft");
		wallRight = GameObject.Find ("WallRight");
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player")
		{
			Grid.enemyManager.enemyList.Remove(this.gameObject);
			SpawnEnemy();
		} else if(objectSpeed < 0 && other.gameObject.name == "WallLeft"){
			Grid.enemyManager.enemyList.Remove(this.gameObject);
			SpawnEnemy();
		}
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (objectSpeed * Time.deltaTime, 0.0f, 0.0f);
		
		if(wallLeft == null && Grid.gameManager.gameStarted)
		{
			wallLeft = GameObject.Find ("WallLeft");
		}
		
		if(wallRight == null && Grid.gameManager.gameStarted)
		{
			wallRight = GameObject.Find ("WallRight");
		}
	}
	
	public void SpawnEnemy()
	{
		int enemySpawnLocation = Grid.gameManager.Multiplier ();
		
		if(enemySpawnLocation < 0)
		{
			Instantiate(enemy, new Vector3(wallLeft.transform.position.x - Random.Range(1.0f, 2.0f), 
			                               wallLeft.transform.position.y + Random.Range (5, 10), wallLeft.transform.position.z),
			            Quaternion.identity);
		}
		
		if(enemySpawnLocation >= 0)
		{
			Instantiate(enemy, new Vector3(wallRight.transform.position.x + Random.Range(1.0f, 2.0f), 
			                               wallRight.transform.position.y + Random.Range (5, 10), wallRight.transform.position.z),
			            Quaternion.identity);
		}
	}
}
