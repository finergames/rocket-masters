﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

	public List<GameObject> enemyList = new List<GameObject>();

	public GameObject enemy;
	//	private Vector3 stageDimensions;
	private GameObject wallLeft, wallRight, wallTop, wallBottom;
	private float objectSpeed = 2.0f;
	private int maxNumEnemies = 3;
	private bool spawned;
	
	void Awake()
	{
		//		stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));
		
		wallLeft = GameObject.Find ("WallLeft");
		wallRight = GameObject.Find ("WallRight");
		wallTop = GameObject.Find ("WallTop");
		wallBottom = GameObject.Find ("WallBottom");
	}
	
	// Use this for initialization
	void Start () 
	{
//		Grid.enemyManager.enemyList.Add (this.gameObject);
		spawned = false;
		
		if(this.transform.position.x > 0.0f)
		{
			objectSpeed = Mathf.Abs(objectSpeed) * -1;
			this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x) * -1, this.transform.localScale.y, 
			                                        this.transform.localScale.z);
		}else if(this.transform.position.x <= 0.0f)
		{
			objectSpeed = Mathf.Abs(objectSpeed);
			this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, 
			                                        this.transform.localScale.z);
		}
	}
	
	void OnEnable()
	{
		//		InvokeRepeating("SpawnEnemy", 1.5f, 1.5f);
	}
	
	void OnDisable()
	{
		//		CancelInvoke("SpawnEnemy");
	}
	
	
	// Update is called once per frame
	void Update () 
	{
//		Debug.Log (spawned);                
		
//		transform.Translate (objectSpeed * Time.deltaTime, 0.0f, 0.0f);
		
		if(wallLeft == null && Grid.gameManager.gameStarted)
		{
			wallLeft = GameObject.Find ("WallLeft");
		}
		
		if(wallRight == null && Grid.gameManager.gameStarted)
		{
			wallRight = GameObject.Find ("WallRight");
		}

		if(wallTop == null && Grid.gameManager.gameStarted)
		{
			wallTop = GameObject.Find ("wallTop");
		}
		
		if(wallBottom == null && Grid.gameManager.gameStarted)
		{
			wallBottom = GameObject.Find ("WallBottom");
		}
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		//		if(objectSpeed > 0 && other.gameObject.name == "WallRight" && !spawned)
		//		{
		//			SpawnEnemy();
		//		} else if(objectSpeed < 0 && other.gameObject.name == "WallLeft" && !spawned){
		//			SpawnEnemy();
		//		}else if(other.gameObject.name == "EnemyDetector" && !spawned){
		//			SpawnEnemy();
		//		}
		
		if(other.gameObject.tag == "Enemy"){
			SpawnEnemy();
		}
	}
	
	public void SpawnEnemy()
	{
		
//		if(Grid.enemyManager.enemyList.Count < maxNumEnemies)
		if(true)
		{
			spawned = true;
			//			Grid.enemyManager.enemyList.Remove(this.gameObject);
			//			int numOfEnemies = Random.Range(1, (maxNumEnemies - Grid.enemyManager.enemyList.Count + 1));
			int numOfEnemies = 1;
			
			for(int i = 0; i < numOfEnemies; i++)
			{
				int enemySpawnLocation = Grid.gameManager.Multiplier ();
				
				if(enemySpawnLocation < 0)
				{
					Instantiate(enemy, new Vector3(wallLeft.transform.position.x - Random.Range(0.0f, 3.0f), 
					                               wallLeft.transform.position.y + Random.Range (7, wallTop.transform.position.y), 
					                               wallLeft.transform.position.z), Quaternion.identity);
				}
				
				if(enemySpawnLocation >= 0)
				{
					Instantiate(enemy, new Vector3(wallRight.transform.position.x + Random.Range(0.0f, 3.0f), 
					                               wallLeft.transform.position.y + Random.Range (7, wallTop.transform.position.y), 
					                               wallLeft.transform.position.z), Quaternion.identity);
				}
			}
		}
		
		//		int numOfEnemies = Random.Range(1, 1);
		//
		//		for(int i = 0; i < numOfEnemies; i++)
		//		{
		//			Instantiate(enemy, new Vector3(Random.Range(wallLeft.transform.position.x, wallRight.transform.position.x), 
		//			                               wallTop.transform.position.y - 5.0f, 0), Quaternion.identity);
		//		}
	}
}
