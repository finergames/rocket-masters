﻿using UnityEngine;
using System.Collections;

public class BackgroundSpawner : MonoBehaviour {

	public GameObject[] skyPrefabs;
	public GameObject[] groundPrefabs;
	private Vector3 stageDimensions;
	
	void Start()
	{
		Vector3 thisCollider = this.GetComponent<Collider2D> ().bounds.size;
		this.transform.position = new Vector3 (0.0f, Grid.levelManager.stageDimensions.y + (thisCollider.y), 0.0f);
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag == "sky")
		{
			SpawnSky(other.transform.position, other.GetComponent<Collider2D>().bounds.size.x);
		}
		
		if(other.tag == "ground")
		{
			SpawnGround(other.transform.position, other.GetComponent<Collider2D>().bounds.size.x);
		}
	}
	
	public void SpawnSky(Vector3 spawnPos, float xPosOffset)
	{
		if(Grid.scoreManager.totalPoints < Grid.levelManager.level1)
		{
			GameObject skyPrefab = skyPrefabs[0];
			GameObject newSky = (GameObject)Instantiate(skyPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		}else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level1 && Grid.scoreManager.totalPoints < Grid.levelManager.level2){
			GameObject skyPrefab = skyPrefabs[1];
			GameObject newSky = (GameObject)Instantiate(skyPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		} else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level2 && Grid.scoreManager.totalPoints < Grid.levelManager.level3){
			GameObject skyPrefab = skyPrefabs[2];
			GameObject newSky = (GameObject)Instantiate(skyPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		} else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level3 && Grid.scoreManager.totalPoints < Grid.levelManager.level4){
			GameObject skyPrefab = skyPrefabs[3];
			GameObject newSky = (GameObject)Instantiate(skyPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		} else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level4){
			GameObject skyPrefab = skyPrefabs[4];
			GameObject newSky = (GameObject)Instantiate(skyPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		}
	}
	
	public void SpawnGround(Vector3 spawnPos, float xPosOffset)
	{
		if(Grid.scoreManager.totalPoints < Grid.levelManager.level1)
		{
			GameObject groundPrefab = groundPrefabs[0];
			GameObject newGround = (GameObject)Instantiate(groundPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		}else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level1 && Grid.scoreManager.totalPoints < Grid.levelManager.level2){
			GameObject groundPrefab = groundPrefabs[1];
			GameObject newGround = (GameObject)Instantiate(groundPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		} else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level2 && Grid.scoreManager.totalPoints < Grid.levelManager.level3){
			GameObject groundPrefab = groundPrefabs[2];
			GameObject newGround = (GameObject)Instantiate(groundPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		} else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level3 && Grid.scoreManager.totalPoints < Grid.levelManager.level4){
			GameObject groundPrefab = groundPrefabs[3];
			GameObject newGround = (GameObject)Instantiate(groundPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		} else if(Grid.scoreManager.totalPoints >= Grid.levelManager.level4){
			GameObject groundPrefab = groundPrefabs[4];
			GameObject newGround = (GameObject)Instantiate(groundPrefab, new Vector3(spawnPos.x + xPosOffset, spawnPos.y, spawnPos.z), Quaternion.identity);
		}
	}
}