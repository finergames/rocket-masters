﻿using UnityEngine;
using System.Collections;

public class DestroyShip : MonoBehaviour {

	public delegate void GameOverAction();
	public static event GameOverAction onGameOver;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			Grid.gameManager.gameStarted = false;
			other.gameObject.SetActive(false);

			if(onGameOver != null)
			{
				onGameOver();
			}
		}
	}
}
