using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	private GameObject wallLeft, wallRight, wallBottom;
	private float objectSpeed = 1.0f;

	void Awake()
	{
//		stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));

		wallLeft = GameObject.Find ("WallLeft");
		wallRight = GameObject.Find ("WallRight");
		wallBottom = GameObject.Find ("WallBottom");
	}

	// Use this for initialization
	void Start () 
	{
//		Grid.enemyManager.enemyList.Add (this.gameObject);

		if(this.transform.position.x > 0.0f)
		{
			objectSpeed = Mathf.Abs(objectSpeed) * -1;
			this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x) * -1, this.transform.localScale.y, 
			                                        this.transform.localScale.z);
		}else if(this.transform.position.x <= 0.0f)
		{
			objectSpeed = Mathf.Abs(objectSpeed);
			this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, 
			                                        this.transform.localScale.z);
		}
	}

	void OnEnable()
	{
//		InvokeRepeating("SpawnEnemy", 1.5f, 1.5f);
	}

	void OnDisable()
	{
//		CancelInvoke("SpawnEnemy");
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject == wallLeft && objectSpeed < 0)
		{
			objectSpeed = objectSpeed * -1;
			this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, 
			                                        this.transform.localScale.z);
		}

		if(other.gameObject == wallRight && objectSpeed > 0)
		{
			objectSpeed = objectSpeed * -1;
			this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, 
			                                        this.transform.localScale.z);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (objectSpeed * Time.deltaTime, 0.0f, 0.0f);

		if(wallLeft == null && Grid.gameManager.gameStarted)
		{
			wallLeft = GameObject.Find ("WallLeft");
		}

		if(wallRight == null && Grid.gameManager.gameStarted)
		{
			wallRight = GameObject.Find ("WallRight");
		}

		if(wallBottom == null && Grid.gameManager.gameStarted)
		{
			wallBottom = GameObject.Find ("WallBottom");
		}
	}
}
