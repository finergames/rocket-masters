﻿using UnityEngine;
using System.Collections;

public class AnimButtonRate : MonoBehaviour, iInputManager {

	private GameObject buttonRateSprite;
	private Animator buttonRateAnim;
	private int buttonRateAnimState;
	private GameObject buttonRateSound1;
	private GameObject buttonRateSound2;
	private bool movedOff;

	void Start ()
	{
		buttonRateSprite = GameObject.Find("ButtonRateSprite");
		buttonRateAnim = buttonRateSprite.GetComponent<Animator>();
		buttonRateSound1 = GameObject.Find ("ButtonRateSound1");
		buttonRateSound2 = GameObject.Find ("ButtonRateSound2");
		movedOff = false;
	}

	#region iInputManager implementation

	public void OnTouchStayed ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchBeganAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchStayedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchMovedAnywhere ()
	{
		if (!movedOff) 
		{
			movedOff = true;
			buttonRateSound2.audio.Play ();
		}

		buttonRateAnimState = 3;
		buttonRateAnim.SetInteger("buttonStates", buttonRateAnimState);
	}

	public void OnTouchMoved ()
	{
		if(movedOff)
		{
			buttonRateSound1.audio.Play ();
			movedOff = false;
		}

		if(buttonRateAnimState != 2)
		{
			buttonRateAnimState = 2;
			buttonRateAnim.SetInteger("buttonStates", buttonRateAnimState);
		}	
	}
	
	public void OnTouchBegan()
	{
		if(movedOff)
		{
			movedOff = false;
		}

		buttonRateSound1.audio.Play ();
		buttonRateAnimState = 2;
		buttonRateAnim.SetInteger("buttonStates", buttonRateAnimState);
	}

	public void OnTouchEnded()
	{
		if(movedOff)
		{
			movedOff = false;
		}

		buttonRateSound2.audio.Play ();
		buttonRateAnimState = 3;
		buttonRateAnim.SetInteger("buttonStates", buttonRateAnimState);

		#if UNITY_IPHONE
		Application.OpenURL ("itms-apps://itunes.apple.com/app/id903672362");
		#endif

		#if UNITY_ANDROID
		Application.OpenURL("market://details?id=com.finergames.freedomtapper");
		#endif
	}

	public void OnTouchEndedAnywhere()
	{
		buttonRateSound2.audio.Play ();
	}

	#endregion

}
