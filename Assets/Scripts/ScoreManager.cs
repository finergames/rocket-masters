﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using CodeStage.AntiCheat.ObscuredTypes;

public class ScoreManager : MonoBehaviour {

	public ObscuredInt totalPoints,
	gameOverPoints, highScore, gameOverHighScore;

	void Awake()
	{
		#if UNITY_IPHONE
		Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
		#endif
		Grid.scoreManager.Load();
		Grid.scoreManager.totalPoints = 0;
		Grid.scoreManager.gameOverPoints = 0;
		Grid.scoreManager.gameOverHighScore = Grid.scoreManager.highScore;
	}

	void Start () 
	{

	}
	
	void Update ()
	{

		if(Grid.scoreManager.highScore < Grid.scoreManager.totalPoints)
		{
			Grid.scoreManager.highScore = Grid.scoreManager.totalPoints;
			//Grid.scoreManager.Save();
			//GameCenterBinding.reportScore(Grid.scoreManager.highScore, "freedomtapper.taps");
		}
	}

	public void AddPoints(int pointsToAdd)
	{
		totalPoints += pointsToAdd;
	}

//	public void Save()
//	{
//		BinaryFormatter bf = new BinaryFormatter();
//		FileStream file = File.Create(Application.persistentDataPath + "/playerData.dat");
//
//		PlayerData data = new PlayerData();
//		data.highScore = Grid.scoreManager.highScore;
//
//		bf.Serialize(file, data);
//		file.Close();
//		#if UNITY_IPHONE
//		GameCenterBinding.reportScore(Grid.scoreManager.highScore, "freedomtapper.taps");
//		#endif
//	}
//
//	public void Load()
//	{
//		if(File.Exists(Application.persistentDataPath + "/playerData.dat"))
//		{
//			BinaryFormatter bf = new BinaryFormatter();
//			FileStream file = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.Open);
//			PlayerData data = (PlayerData) bf.Deserialize(file);
//			file.Close();
//
//			Grid.scoreManager.highScore = data.highScore;
//		}
//	}

	public void Save()
	{
		if(Grid.scoreManager.highScore > ObscuredPrefs.GetInt("highScore"))
		{
			ObscuredPrefs.SetInt ("highScore", Grid.scoreManager.highScore);
		}

		#if UNITY_IPHONE
		GameCenterBinding.reportScore(ObscuredPrefs.GetInt("highScore"), "freedomtapper.taps");
		#endif

		#if UNITY_ANDROID
		PlayGameServices.submitScore("CgkIoMLlsYoFEAIQAA", ObscuredPrefs.GetInt("highScore"));
		#endif
	}
	
	public void Load()
	{
		Grid.scoreManager.highScore = ObscuredPrefs.GetInt ("highScore");
	}

	void OnEnable()
	{
		Grid.scoreManager.Load();
	}

	void OnDisable()
	{
		Grid.scoreManager.Save();
	}
}

//[Serializable]
//class PlayerData
//{
//	public int highScore;
//}
