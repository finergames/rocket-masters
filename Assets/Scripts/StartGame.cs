﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour, iInputManager {

	private GameObject tapToStartOverlay;
	public GameObject enemy;

//	public delegate void TapAction ();
//	public static event TapAction OnTapped;

	#region iInputManager implementation
	public void OnTouchBegan ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchEnded ()
	{
		Grid.gameManager.gameStarted = true;
		tapToStartOverlay.SetActive(false);

//		if(OnTapped != null)
//		{
//			OnTapped();
//		}
//		enemySpawner.GetComponent<EnemySpawner> ().enabled = true;
	}
	public void OnTouchMoved ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchStayed ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchBeganAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchEndedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchMovedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchStayedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	#endregion

	void Awake()
	{
		tapToStartOverlay = GameObject.Find ("TapToStartOverlay");
		Instantiate(enemy, new Vector3(Random.Range (-4, 4), Random.Range(0, 10), 0), Quaternion.identity);
		Instantiate(enemy, new Vector3(Random.Range (-4, 4), Random.Range(0, 10), 0), Quaternion.identity);
	}
	// Use this for initialization
	void Start () 
	{
		if(Grid.enemyManager.enemyList.Count == 0)
		{

		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(tapToStartOverlay == null)
		{
			tapToStartOverlay = GameObject.Find ("TapToStartOverlay");
		}
	}
}
