﻿using UnityEngine;
using System.Collections;

public class GameOverScreenAnimator : MonoBehaviour {

	private int gameOverAnimState;

	// Use this for initialization
	void Start () {
		gameOverAnimState = 0;
		this.GetComponent<Animator>().SetInteger ("gameOverState", gameOverAnimState);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		DestroyShip.onGameOver += Animate;
	}

	void OnDisable()
	{
		DestroyShip.onGameOver -= Animate;
	}

	void Animate()
	{
		gameOverAnimState = 1;
		this.GetComponent<Animator>().SetInteger ("gameOverState", gameOverAnimState);
	}
}
