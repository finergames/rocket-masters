﻿using UnityEngine;
using System.Collections;

public class LoadMainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		if (Application.loadedLevelName == "SplashScreen")
		{
			//this.GetChild
			StartCoroutine(AnimateSplashScreen());
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	IEnumerator AnimateSplashScreen()
	{
		yield return new WaitForSeconds(3.0f);
		Grid.levelManager.sceneToLoad = "MainMenu";
		Grid.levelManager.sceneEnding = true;
	}
}
