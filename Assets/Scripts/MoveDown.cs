﻿using UnityEngine;
using System.Collections;

public class MoveDown : MonoBehaviour {
	
	private float objectSpeed = -3.0f;
	
	void Start () 
	{

	}
	
	void Update () {

		if (Grid.gameManager.gameStarted) 
		{
			transform.Translate (0.0f, objectSpeed * Time.deltaTime, 0.0f);
		}
	}
}