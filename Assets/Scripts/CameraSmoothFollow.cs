﻿using UnityEngine;
using System.Collections;

public class CameraSmoothFollow : MonoBehaviour {
	
	public GameObject cameraTarget; // object to look at or follow
	public float smoothTime = 0.1f; // time for dampen
	public bool cameraFollowX = true; // camera follows on horizontal
	public bool cameraFollowY = true; // camera follows on vertical
	public float cameraHeight = 2.5f; // height of camera adjustable
	public Vector2 velocity; // speed of camera movement
	private float posX, posY;
	
	// Use this for initialization
	void Start () 
	{
		cameraTarget = GameObject.FindGameObjectWithTag ("Player");
		posX = cameraTarget.transform.position.x;
		posY = cameraTarget.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(cameraTarget == null && Grid.gameManager.gameStarted)
		{
			cameraTarget = GameObject.FindGameObjectWithTag ("Player");
		}

		if(cameraTarget != null)
		{
			if (cameraFollowX)
			{
				posX = Mathf.SmoothDamp(posX, cameraTarget.transform.position.x, ref velocity.x, smoothTime);
			}

			if (cameraFollowY) 
			{
				posY = Mathf.SmoothDamp(posY, cameraTarget.transform.position.y + cameraHeight, ref velocity.x, smoothTime);
			}

			this.transform.position = new Vector3 (posX, posY, this.transform.position.z);
		}
	}
}